using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour
{
    public static EventManager Instance;

    private void Awake()
    {
        Instance = this;
    }

    public event Action OnUpdatePlayerList;

    public void UpdatePlayerList()
    {
        OnUpdatePlayerList?.Invoke();
    }

    public event Action<bool> OnPvpStart;

    public void PvpStart(bool statement)
    {
        OnPvpStart?.Invoke(statement);
    }
}
