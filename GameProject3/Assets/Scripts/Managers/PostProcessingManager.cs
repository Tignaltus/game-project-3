using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.UI;

public class PostProcessingManager : MonoBehaviour
{
    private Volume postProcessing;
    private ColorAdjustments colorAdjustments;
    private Bloom bloom;
    private Vignette vignette;
    private MotionBlur motionBlur;

    [TabGroup("Menu")] 
    [SerializeField] private Slider exposureSlider;
    [TabGroup("Menu")] 
    [SerializeField] private TMP_Text exposureText;
    [TabGroup("Menu")] 
    [SerializeField] private Slider contrastSlider;
    [TabGroup("Menu")] 
    [SerializeField] private TMP_Text contrastText;
    [TabGroup("Menu")] 
    [SerializeField] private Slider BloomSlider;
    [TabGroup("Menu")] 
    [SerializeField] private TMP_Text BloomText;
    [TabGroup("Menu")] 
    [SerializeField] private Slider VignetteSlider;
    [TabGroup("Menu")] 
    [SerializeField] private TMP_Text VignetteText;
    [TabGroup("Menu")] 
    [SerializeField] private TMP_Dropdown dropdown;
    


    private void Awake()
    {
        DontDestroyOnLoad(this);
        postProcessing = GetComponent<Volume>();
        postProcessing.profile.TryGet(out colorAdjustments);
        postProcessing.profile.TryGet(out bloom);
        postProcessing.profile.TryGet(out vignette);
        postProcessing.profile.TryGet(out motionBlur);
    }

    public void ChangeExposure()
    {
        colorAdjustments.postExposure.value = exposureSlider.value;
        exposureText.text = exposureSlider.value.ToString("F1");
    }

    public void ChangeContrast()
    {
        colorAdjustments.contrast.value = contrastSlider.value;
        contrastText.text = contrastSlider.value.ToString("00");
    }
    
    public void ChangeBloom()
    {
        bloom.intensity.value = BloomSlider.value;
        BloomText.text = BloomSlider.value.ToString("F1");
    }
    
    public void ChangeVignette()
    {
        vignette.intensity.value = VignetteSlider.value;
        VignetteText.text = VignetteSlider.value.ToString("F1");
    }

    public void ChangeMotionBlur(bool statement)
    {
        motionBlur.active = statement;
    }

    public void ChangeFullScreen()
    {
        switch (dropdown.value)
        {
            case 0:
                Screen.fullScreen = true;
                break;
            case 1:
                Screen.fullScreen = false;
                break;
        }
    }
}
