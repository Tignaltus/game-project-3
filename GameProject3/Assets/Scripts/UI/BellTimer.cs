using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BellTimer : MonoBehaviourPunCallbacks
{
    [SerializeField] private TMP_Text timerText;
    [SerializeField] private TMP_Text startTimer;
    [SerializeField] private TMP_Text warningText;
    [SerializeField] private float secondsBeforeStart;
    [SerializeField] private GameObject barrier;
    [SerializeField] private GameObject warningBell;
    private float seconds;
    [SerializeField] private float minutes = 10f;
    [Tooltip("In minutes")]
    [SerializeField] private float timeToArenaMin = 0f;
    [Tooltip("In seconds")]
    [SerializeField] private float timeToArenaSec = 30f;
    [SerializeField] private AudioClip bellSound;
    [SerializeField] private AudioClip battleMusic;
    [SerializeField] private CheckIfInArena arenaCheck;
    [SerializeField] private GameObject infoCanvas;

    public bool matchStart;
    private bool bellRing;

    private void Start()
    {
        DontDestroyOnLoad(gameObject);
        infoCanvas.SetActive(true);
    }

    public void StartCounter()
    {
        StartCoroutine(MatchCountdown());
    }

    private IEnumerator TimerCount()
    {
        EventManager.Instance.PvpStart(false);

        while (minutes >= 0)
        {
            seconds--;
            if (seconds < 0)
            {
                seconds = 59f;
                minutes--;
            }

            if (seconds < 10)
            {
                timerText.text = minutes + ":" + 0 + seconds;
            }
            else
            { 
                timerText.text = minutes + ":" + seconds;
            }

            if (minutes <= timeToArenaMin && seconds <= timeToArenaSec && !bellRing)
            {
                bellRing = true;
                BellRing();
            }
            yield return new WaitForSecondsRealtime(1f);
        }
        
        warningText.gameObject.SetActive(false);
        warningBell.SetActive(false);
        timerText.gameObject.SetActive(false);
        startTimer.gameObject.SetActive(false);
        EventManager.Instance.PvpStart(true);
        barrier.SetActive(true);
        if (!arenaCheck.InsideCircle())
        {
            PlayerController.LocalPlayerInstance.gameObject.transform.position = GameManager.Instance.spawnPoint.position;
        }

        AudioManager.instance.PlayMusic(battleMusic, 0.7f);
        GameManager.Instance.arenaPhase = true;
        
    }

    private IEnumerator MatchCountdown()
    {
        barrier.SetActive(true);
        seconds = secondsBeforeStart;
        while (seconds > 0)
        {
            seconds--;
            startTimer.text = "Starts in: " + seconds;
            yield return new WaitForSecondsRealtime(1f);
        }

        infoCanvas.SetActive(false);
        matchStart = true;
        barrier.SetActive(false);
        startTimer.gameObject.SetActive(false);
        StartCoroutine(TimerCount());
    }

    private void BellRing()
    {
        AudioManager.instance.PlayBell(bellSound);
        warningText.gameObject.SetActive(true);
        warningBell.SetActive(true);
    }
}
