using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VictoryScreen : MonoBehaviour
{
    [SerializeField]private TMP_Text victoryText;
    [SerializeField] private TMP_Text congratulationText;

    public void GetChampion(string Nickname)
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        victoryText.text = Nickname + " IS THE WINNER!";
        congratulationText.text = Nickname + " is the champion of the Arena!";
    }

    public void ClickAnywhere()
    {
        Debug.Log("CLICKED");
        PhotonNetwork.Disconnect();
        SceneManager.LoadScene(0);
    }
}
