using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomItem : MonoBehaviour
{
    public Text roomName;
    private LobbyManager lobbyManager;

    private void Start()
    {
        lobbyManager = FindObjectOfType<LobbyManager>();
    }

    public void SetRoomName(string _roomname)
    {
        roomName.text = _roomname;
    }

    public void OnClickRoom()
    {
        lobbyManager.JoinRoom(roomName.text);
    }
}
