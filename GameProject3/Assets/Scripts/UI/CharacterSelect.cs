using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSelect : MonoBehaviour
{
    public List<Button> buttons;
    public List<GameObject> characterPrefab;
    public int[] character;
    public int index;

    private void Start()
    {
        index = PlayerPrefs.GetInt("Character");
        buttons[index].Select();
        SelectCharacter(index);
    }

    public void OnSelectCharacter(int i)
    {
        index = i;
        SelectCharacter(index);
    }

    public void OnArrowClick(bool forward)
    {
        switch (forward)
        {
            case true:
                index = (index + 1) % character.Length;
                SelectCharacter(index);
                break;
            case false:
                index = (index - 1) % character.Length;
                if (index < 0) index = character.Length - 1;
                SelectCharacter(index);
                break;
        }
        
        buttons[index].Select();
        Debug.Log("Clicked arrow");
    }

    private void SelectCharacter(int cIndex)
    {
        foreach (var GO in characterPrefab)
        {
            GO.SetActive(false);
        }
        characterPrefab[cIndex].SetActive(true);
        PlayerPrefs.SetInt("Character", cIndex);
    }
}
