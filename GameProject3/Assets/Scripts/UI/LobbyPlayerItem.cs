using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LobbyPlayerItem : MonoBehaviour
{
    public TMP_Text playerName;
    private LobbyManager lobbyManager;

    private void Start()
    {
        lobbyManager = FindObjectOfType<LobbyManager>();
    }

    public void SetPlayerName(string _playername)
    {
        playerName.text = _playername;
    }

    public void OnClickRoom()
    {
        lobbyManager.JoinRoom(playerName.text);
    }
}
