using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using Sirenix.OdinInspector;

[RequireComponent(typeof(LineOfSight))]
public class AggroTarget : MonoBehaviourPunCallbacks, IPunObservable
{
    [Serializable]
    public class AggroToUnit
    {
        public GameObject player;
        public float aggro;
    }

    public AggroToUnit currentTarget;
    private AggroToUnit tempUnit;
    [SerializeField]private List<AggroToUnit> playerList;
    [SerializeField]private LineOfSight lineOfSight;

    [Tooltip("If the aggro of an player goes above this the AI will perma chase the player")]
    public float angryThreshold = 50f;

    public bool isAngry;

    private void Awake()
    {
        lineOfSight = GetComponent<LineOfSight>();
    }

    private void OnEnable()
    {
        EventManager.Instance.OnUpdatePlayerList += UpdatePlayerList;
    }

    private void OnDisable()
    {
        EventManager.Instance.OnUpdatePlayerList -= UpdatePlayerList;
    }

    private void Start()
    {
        StartCoroutine(UnitToAttack());
        UpdatePlayerList();
    }

    //Clears the list so no aggro is saved. Works for our game tho...
    private void UpdatePlayerList()
    {
        playerList.Clear();
        foreach (var user in GameManager.Instance.spawnedPlayers)
        {
            var clientPlayer = new AggroToUnit {player = user};
            //if (playerList.Contains(clientPlayer)) continue;
            playerList.Add(clientPlayer);
        }
    }
    
    /// <summary>
    /// Checks if a player is visible for the goblin if it is the function goes thru its playerlist to see what player it sees.
    /// It then checks if its currentTarget is null or that the unit/player that its seeing has more aggro then the currentTarget.
    /// If it has it calls a server callback to change the currentTarget on all clients to that unit/player.
    /// </summary>
    private IEnumerator UnitToAttack()
    {
        while (true)
        {
            if (lineOfSight.visibleTargets.Count > 0)
            {
                foreach (var target in lineOfSight.visibleTargets)
                {
                    foreach (var player in playerList)
                    {
                        if (target == player.player)
                        {
                            if (player.player == currentTarget.player &&
                                !player.player.GetComponent<PlayerController>().isAlive)
                            {
                                currentTarget.aggro = 0f;
                                player.aggro = 0f;
                                isAngry = false;
                            }
                                
                            if (currentTarget.player == null || currentTarget.aggro < player.aggro)
                            {
                                photonView.RPC("RPCCurrentTargetChange", RpcTarget.AllViaServer, player.player.GetComponent<PhotonView>().ViewID, player.aggro);
                            }
                        }
                    }
                }
            }
            yield return new WaitForSeconds(.2f);
        }
    }

    /// <summary>
    /// Called from the players class to increase the aggro towards that player. Once called it goes thru its playerList
    /// to check which unit is calling it. Once found it increases the aggro in the AggroUnit object based on the amount
    /// sent by the player. It then checks if the aggro towards that player is higher then the angryThreshold and if the
    /// AI is not angry. If both is true sets angry to true and calls a server callback to change the currentTarget on the
    /// AI to the same on all clients.
    /// </summary>
    /// <param name="player"></param>
    /// <param name="amount"></param>
    public void IncreaseAggro(GameObject player, float amount)
    {
        foreach (var unit in playerList)
        {
            if (unit.player == player)
            {
                unit.aggro += amount;
                if (unit.aggro > angryThreshold && !isAngry)
                {
                    photonView.RPC("RPCCurrentTargetChange", RpcTarget.AllViaServer, unit.player.GetComponent<PhotonView>().ViewID, unit.aggro);
                    isAngry = true;
                }
            }
        }
    }

    //Updates the currentTarget thru the server. Sends an int and float instead of an object because photon only allows
    //a certain amount of data to be transfered in the parameters.
    [PunRPC]
    private void RPCCurrentTargetChange(int viewID, float aggro)
    {
        currentTarget.player = PhotonView.Find(viewID).gameObject;
        currentTarget.aggro = aggro;
        //currentTarget = tempUnit;
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        
    }
}
