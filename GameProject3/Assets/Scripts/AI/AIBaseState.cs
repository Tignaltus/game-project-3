using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public abstract class AIBaseState
{
    public abstract void EnterState(AIEntity AI);

    public abstract void Update(AIEntity AI);

    public abstract void OnCollisionEnter(AIEntity AI);

    //Used as an extra function. Can be used for miscellaneous functions that is specific to that state.
    public abstract void MiscFunction(AIEntity AI, Vector3 position);
}
