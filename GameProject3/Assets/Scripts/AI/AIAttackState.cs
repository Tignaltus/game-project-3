using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIAttackState : AIBaseState
{
    public override void EnterState(AIEntity AI)
    {
        Debug.Log("Entered AttackState");
    }

    /// <summary>
    /// Checks if the AI has a target if false it goes back to PatrolState. If true it rotates towards the target
    /// and calls for the attack coroutine in which it deals damage to the player if canAttack is true.
    /// If the target is outside attack range it goes back to ChaseState.
    /// </summary>
    /// <param name="AI"></param>
    public override void Update(AIEntity AI)
    {
        if (AI.lineOfSight.visibleTargets.Count == 0) AI.TransitionToState(AI.PatrolState);
        else
        {
            AI.RotateAI(AI.aggroTarget.currentTarget.player.transform.position);
            
            if (AI.canAttack)
            {
                AI.canAttack = false;
                Debug.Log("attack");
                AI.StartCoroutine(Attack(AI));
            }
            
            if (Vector3.Distance(AI.transform.position, AI.aggroTarget.currentTarget.player.transform.position) >=
                AI.attackRange)
            {
                AI.TransitionToState(AI.ChaseState);
            }
        }
    }

    public override void OnCollisionEnter(AIEntity AI)
    {
        
    }

    public override void MiscFunction(AIEntity AI, Vector3 position)
    {
        
    }

    private IEnumerator Attack(AIEntity AI)
    {
        AI.aggroTarget.currentTarget.player.GetComponent<IDamageable>().TakeDamage(AI.damage, AI.gameObject);
        AI.anim.SetTrigger("Attack");
        if(AI.audio.InAudioRange()) AudioManager.instance.PlaySFX(AI.audio.attack, 1f);
        Debug.Log("AI Attack!!!!!");
        yield return new WaitForSeconds(AI.attackRate);
        AI.canAttack = true;
    }
}
