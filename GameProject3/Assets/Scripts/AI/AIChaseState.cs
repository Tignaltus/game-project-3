using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class AIChaseState : AIBaseState
{
    private Vector3 velocityDamp = Vector3.zero;
    private bool isStuck;
    public override void EnterState(AIEntity AI)
    {
        Debug.Log("ChaseState");
        AI.anim.SetTrigger("Run");
    }
    
    public override void Update(AIEntity AI)
    {
        if ((AI.lineOfSight.visibleTargets.Count > 0 || AI.aggroTarget.isAngry) && AI.aggroTarget.currentTarget.player != null)
        {
            if (isStuck){AI.rb.velocity = (-AI.transform.forward + AI.transform.right).normalized * (AI.moveSpeed * Time.deltaTime); return;}
            
            var Targetpos = AI.aggroTarget.currentTarget.player.transform.position;
            
            AI.RotateAI(Targetpos);
            //AI.rb.velocity = Vector3.zero;
            AI.dirToTarget = (Targetpos - AI.transform.position).normalized;
            //AI.rb.AddRelativeForce(AI.dirToTarget * AI.moveSpeed, ForceMode.Impulse);
            
            //Pathfinding
            if (AI.ForwardRaycast() || isStuck)
            {
                /*AI.dirToTarget = -AI.transform.forward + AI.transform.right;
                AI.dirToTarget = Vector3.SmoothDamp(AI.transform.position, AI.dirToTarget, ref velocityDamp, 0.15f).normalized;*/
                Debug.Log("stuck on obstacle");
                AI.dirToTarget = (new Vector3(AI.dirToTarget.x, 0f, 0f) + AI.transform.right).normalized;
                Debug.Log(AI.dirToTarget);
                Debug.DrawLine(AI.transform.position, AI.dirToTarget, Color.red);
            }
            if(AI.flying) AI.rb.velocity = AI.dirToTarget * (AI.moveSpeed * Time.deltaTime);
            else AI.rb.velocity = new Vector3(AI.dirToTarget.x, 0f, AI.dirToTarget.z) * (AI.moveSpeed * Time.deltaTime);

            if (Vector3.Distance(AI.transform.position, Targetpos) < AI.attackRange && AI.lineOfSight.visibleTargets.Count > 0)
            {
                AI.TransitionToState(AI.AttackState);
            }
        }
        else
        {
            AI.TransitionToState(AI.PatrolState);
        }
    }

    public override void OnCollisionEnter(AIEntity AI)
    {

        if (!isStuck)
        {
            isStuck = true;
            AI.StartCoroutine(UnStuck());
        }
    }

    public override void MiscFunction(AIEntity AI, Vector3 position)
    {
        
    }

    private IEnumerator UnStuck()
    {
        yield return new WaitForSeconds(0.5f);
        isStuck = false;
    }
}
