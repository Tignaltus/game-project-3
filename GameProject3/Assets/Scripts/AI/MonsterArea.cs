using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MonsterArea : MonoBehaviour
{
    [SerializeField]public float AreaRadius = 8f;

    private void OnDrawGizmos()
    {
        Handles.DrawWireDisc(transform.position, Vector3.up, AreaRadius, 2f);
    }
}
