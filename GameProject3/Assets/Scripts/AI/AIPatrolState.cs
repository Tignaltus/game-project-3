using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class AIPatrolState : AIBaseState
{
    private Vector3 patrolPoint;
    private bool patrolling;
    public override void EnterState(AIEntity AI)
    {
        Debug.Log("Patrol state");
        patrolling = true;
        /*if (PhotonNetwork.IsMasterClient)
        {
            AI.StartCoroutine(SetPatrolPoint(AI));
        }*/

        if (!AI.startPoint)
        {
            //AI.StartCoroutine(SetPatrol(AI));
            AI.StartCoroutine(SetPatrolPoint(AI));
        }
    }

    public override void Update(AIEntity AI)
    {
        AI.RotateAI(patrolPoint);
        
        if (AI.lineOfSight.visibleTargets.Count > 0 || AI.aggroTarget.isAngry)
        {
            patrolling = false;
           // AI.StopCoroutine(SetPatrolPoint(AI));
            AI.TransitionToState(AI.ChaseState);
        }
        Debug.DrawLine(AI.transform.position, patrolPoint, Color.blue);

        if (patrolling && Vector3.Distance(AI.transform.position, patrolPoint) < 1f) 
        {
            patrolling = false;
            AI.anim.SetTrigger("Idle");
        }
        else
        {
            AI.transform.position = Vector3.MoveTowards(AI.transform.position, patrolPoint, 
                AI.patrolSpeed * Time.deltaTime);
            patrolling = true;
        }
    }

    public override void OnCollisionEnter(AIEntity AI)
    {
        
    }

    //Is called thru an RPC to make it a network call. Photon is weird...
    public override void MiscFunction(AIEntity AI, Vector3 position)
    {
        patrolPoint = position;
        AI.startPoint = true;
    }

    /// <summary>
    /// Originally this was used with a while loop to get a new patrol position every set second. Reworked it to just give
    /// a position once to give all ai:s an unique rotations.
    /// </summary>
    /// <param name="AI"></param>
    /// <returns></returns>
    private IEnumerator SetPatrolPoint(AIEntity AI)
    {
        /*yield return new WaitForSeconds(2f);
        if (AI.photonView.IsMine)
        {
            //patrolPoint = new Vector3(AI.monsterAreaT.x, AI.transform.position.y, AI.monsterAreaT.z);
            
            //patrolPoint = new Vector3(Random.Range(AI.transform.position.x, AI.transform.position.x + 2f), AI.transform.position.y,
                Random.Range(AI.transform.position.z, AI.transform.position.z + 2f));
            
            //AI.photonView.RPC("RPCSetPatrolPoint", RpcTarget.AllViaServer, patrolPoint);
        }*/

        yield return new WaitForSeconds(1f);
        AI.anim.SetTrigger("Idle");
        patrolPoint = AI.transform.position;
        AI.startPoint = true;
    }
}
