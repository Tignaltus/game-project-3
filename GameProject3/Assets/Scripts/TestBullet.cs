using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class TestBullet : MonoBehaviour
{
    [SerializeField] private Rigidbody rb;
    public Transform playerT;

    private void Start()
    {
        Physics.IgnoreCollision(playerT.GetComponent<Collider>(), GetComponent<Collider>());
        rb.AddForce(playerT.forward * 100f, ForceMode.Impulse);
        Destroy(this, 5f);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<IDamageable>(out var damageable))
        {
            damageable.TakeDamage(5, playerT.gameObject);
        }

        if (other.TryGetComponent<AggroTarget>(out var aggroTarget))
        {
            aggroTarget.IncreaseAggro(playerT.gameObject, 10);
        }
        Debug.Log("damage");
    }

    private void OnDestroy()
    {
        PhotonNetwork.Destroy(gameObject);
    }
}
