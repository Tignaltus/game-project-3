using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LobbyCamera : MonoBehaviour
{
    public List<Transform> positions;
    [Range(0.001f, 0.1f)]
    public float speed = 0.01f;

    private bool move = false;
    private bool moveback = false;

    public void Update()
    {
        if (move)
        {
            transform.position = Vector3.Slerp(transform.position, positions[1].position, speed);
            transform.rotation = Quaternion.Slerp(transform.rotation, positions[1].rotation, speed);
        }

        if (moveback)
        {
            transform.position = Vector3.Slerp(transform.position, positions[0].position, speed);
            transform.rotation = Quaternion.Slerp(transform.rotation, positions[0].rotation, speed);
        }
    }

    public void GoToPosition()
    {
        moveback = false;
        move = true;
    }

    public void GoBack()
    {
        move = false;
        moveback = true;
    }
}
