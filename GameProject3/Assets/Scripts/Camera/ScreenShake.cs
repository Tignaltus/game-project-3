using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class ScreenShake : MonoBehaviour
{
    public float duration, magnitude, zoom;
    [ContextMenu("Shake")]
    public void ShakeCamera()
    {
        StartCoroutine(Shake());
    }
    
    private IEnumerator Shake()
    {
        Vector3 originalPos = transform.localPosition;

        float elapsed = 0.0f;

        while (elapsed < duration)
        {
            float x = Random.Range(-1f, 1f) * magnitude;
            float y = Random.Range(-1f, 1f) * magnitude;
            float z = Random.Range(-1f, 1f) * zoom;

            transform.localPosition = new Vector3(x, y, z);

            elapsed += Time.deltaTime;
            
            yield return null;
        }

        transform.localPosition = Vector3.zero;
    }
}
