using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingCamera : MonoBehaviour
{
    [SerializeField][Range(1, 25)] private int XZMoveSpeed;
    [SerializeField][Range(1, 25)] private int YMoveSpeed;
    
    private void Update()
    {
        PlayerInput();
    }

    private void PlayerInput()
    {
        if (Input.GetKey(KeyCode.W))
        {
            transform.position += transform.forward * XZMoveSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.position += -transform.right * XZMoveSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.position += -transform.forward * XZMoveSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.position += transform.right * XZMoveSpeed * Time.deltaTime;
        }
        
        
        
        if (Input.GetKey(KeyCode.Space))
        {
            transform.position += transform.up * YMoveSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.LeftShift))
        {
            transform.position += -transform.up * YMoveSpeed * Time.deltaTime;
        }
    }
}
