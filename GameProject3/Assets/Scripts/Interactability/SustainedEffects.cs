using System.Collections;
using System.Collections.Generic;
using Photon.Realtime;
using UnityEngine;

[CreateAssetMenu(fileName = "New Sustained Effect Ability", menuName = "Create New Ability/Sustained Effect")]
public class SustainedEffects : Ability
{
    private int amount;

    public void Initialize(PlayerController subject, Ability abilityData)
    {
        
    }

    IEnumerator TickDamage(PlayerController hitCharacter, int duration)
    {
        duration--;
        
        if (duration > 0)
        {
            //hitCharacter.TakeDamage(amount);
            yield return new WaitForSeconds(1);
            //StartCoroutine(TickDamage(hitCharacter, duration));
        }
        else
        {
            EndEffect();
        }
    }

    IEnumerator TickStun(PlayerController hitCharacter, int duration)
    {
        yield break;
    }

    IEnumerator TickHealing(PlayerController hitCharacter, int duration)
    {
        yield break;
    }
    
    IEnumerator TickShielding(PlayerController hitCharacter, int duration)
    {
        yield break;
    }

    private void EndEffect()
    {
        Destroy(this);
    }
}
