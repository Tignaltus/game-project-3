using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityContainer : MonoBehaviour
{
    [SerializeField] private Ability myAbility;

    public Ability Ability
    {
        get => myAbility;
        set { myAbility = value; }
    }
    
}
