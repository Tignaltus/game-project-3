﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ThirdPersonCamera : MonoBehaviour
{
    public Transform target;
    private Camera mainCamera;
    public bool startMoving;

    [SerializeField] private float offset = 1;
    [SerializeField] private float rotationSpeed = 100f, rotationOffset = 5f;

    private void Start()
    {
        mainCamera = gameObject.GetComponent<Camera>();
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void LateUpdate()
    {        
        //Use the same position as "target"
        if (!startMoving) return;
        if (!target) return;
        transform.position = target.position;

        //If the player is moving the mouse
        if(Input.GetAxis("Mouse X") != 0)
        {
            SphericMovement(new Vector3(0, Input.GetAxis("Mouse X"), 0));
        }

        if(Input.GetAxis("Mouse Y") != 0)
        {
            SphericMovement(new Vector3(Input.GetAxis("Mouse Y"), 0, 0));
        }

        //Use "offset" to push back Camera
        Vector3 pushBackVector = -transform.forward * offset;
        transform.position = transform.position + pushBackVector;

        //Set Z Rotation Axis to 0
        transform.rotation = Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y, 0);

        //Rotation of character
        Vector3 cameraDirection = new Vector3(mainCamera.transform.forward.x, 0f, mainCamera.transform.forward.z);
        Vector3 playerDirection = new Vector3(target.forward.x, 0f, target.forward.z);

        if (Vector3.Angle(cameraDirection, playerDirection) > rotationOffset)
        {
            var targetRotation = Quaternion.LookRotation(cameraDirection, transform.up);
            target.rotation = Quaternion.RotateTowards(target.rotation, targetRotation, rotationSpeed * Time.deltaTime);
        }
    }

    private void SphericMovement(Vector3 direction)
    {
        //Rotate Camera with how the Mouse moves
        Quaternion rotation = Quaternion.Euler(direction.x, direction.y, 0);
        gameObject.transform.rotation = transform.rotation * rotation;
    }
}
