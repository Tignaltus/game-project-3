using System.Collections;
using UnityEngine;
using Photon.Pun;
using Sirenix.OdinInspector;

public class Wizard : PlayerController
{
    [SerializeField] private LineOfSight lineOfSight;
    
    [SerializeField] private Ability primaryAttack;
    [SerializeField] private Ability specialAttack;
    [SerializeField] private Ability ultimateAbility;
    [TabGroup("Wizard")] 
    [SerializeField] private float ultChannelTime;
    [TabGroup("Wizard")] 
    [SerializeField] private float specialChannelT;
    [TabGroup("Wizard")]
    [SerializeField] private ParticleSystem fireStormEffect;
    private Projectile spell;

    private bool specialAttackCD;
    private bool evasivemanouverCD;
    private bool channeling;

    private int jumpCounter;

    public override void PassiveAbility()
    {
        
    }
    
    public override void PrimaryAttack(Transform playerTransform)
    {
        if (channeling) return;
        comboAnims.StartCombo();
    }

    public void Attack()
    {
        rotateAttack = true;
        photonView.RPC("AttackRPC", RpcTarget.AllViaServer);
        AudioManager.instance.PlaySFX(audio.attack, GetRandomPitch());
    }

    [PunRPC]
    public void AttackRPC()
    {
        GameObject target = GetClosestCharacter();
        
        if (target != null)
        {
            GetComponent<AbilityFactory>().FireAbility(primaryAttack, this, target.transform.position);
        }
        else
        {
            GetComponent<AbilityFactory>().FireAbility(primaryAttack, this, transform.position + transform.forward);
        }
    }

    public override void SpecialAttack()
    {
        if (photonView.IsMine && !specialAttackCD)
        {
            StartCoroutine(CooldownSpecial(specialAttack.cooldown));
            StartCoroutine(ChannelSpell(specialChannelT, true));
            photonView.RPC("SpecialAttackRPC", RpcTarget.AllViaServer);
            AudioManager.instance.PlaySFX(audio.specialAttack, 1f);
        }
        
    }
    
    [PunRPC]
    public void SpecialAttackRPC()
    {
        animator.SetTrigger("Shoot");

        GetComponent<AbilityFactory>().FireAbility(specialAttack, this, transform.position + transform.forward);
    }

    public override void EvasiveManeuver()
    {
        if (photonView.IsMine && !evasivemanouverCD)
        {
            StartCoroutine(CooldownUlt(ultimateAbility.cooldown));
            StartCoroutine(ChannelSpell(ultChannelTime, false));
            photonView.RPC("UltimateAttackRPC", RpcTarget.AllViaServer);
            AudioManager.instance.PlaySFX(audio.EvasiveManouver, 1f);
        }
    }

    [PunRPC]
    public void UltimateAttackRPC()
    {
        animator.SetTrigger("Shoot");
        
        GetComponent<AbilityFactory>().FireAbility(ultimateAbility, this, transform.position + transform.forward);
    }

    public override void Jump()
    {
        base.Jump();
    }

    private GameObject GetClosestCharacter()
    {
        if (lineOfSight.visibleTargets.Count > 0)
        {
            GameObject closestTarget = lineOfSight.visibleTargets[0];
    
            foreach (GameObject visibleTarget in lineOfSight.visibleTargets)
            {
                if (Vector3.Distance(visibleTarget.transform.position, transform.position) < Vector3.Distance(closestTarget.transform.position, transform.position))
                {
                    closestTarget = visibleTarget;
                }
            }
    
            return closestTarget;
        }

        return null;
    }

    private IEnumerator CooldownSpecial(float seconds)
    {
        specialAttackCD = true;
        yield return new WaitForSeconds(seconds);
        specialAttackCD = false;
    }
    
    private IEnumerator CooldownUlt(float seconds)
    {
        evasivemanouverCD = true;
        yield return new WaitForSeconds(seconds);
        evasivemanouverCD = false;
    }

    private IEnumerator ChannelSpell(float channelTime, bool withEffect)
    {
        moveDir = Vector3.zero;
        if(withEffect)
        {
            var effect = Instantiate(fireStormEffect);
            effect.transform.position = transform.position;
            Destroy(effect.gameObject, channelTime);
        }
        animator.SetBool("ChannelSpell", true);
        customInput = true;
        channeling = true;
        yield return new WaitForSeconds(channelTime);
        animator.SetBool("ChannelSpell", false);
        customInput = false;
        channeling = false;
    }
}
