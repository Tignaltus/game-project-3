using UnityEngine;

public class ModelRotation : MonoBehaviour
{
    [SerializeField]private PlayerController playerC;
    public bool dontRotate;

    private void Start()
    {
        playerC = GetComponentInParent<PlayerController>();
    }

    private void FixedUpdate()
    {
        if(dontRotate) return;
        
        if (!(playerC.moveAmount.magnitude >= 0.1f) || playerC.isStunned) return;
        float targetAngle = Mathf.Atan2(playerC.moveAmount.x, playerC.moveAmount.z) * Mathf.Rad2Deg;
        transform.localRotation = Quaternion.Euler(0f, targetAngle, 0f);
    }
}
