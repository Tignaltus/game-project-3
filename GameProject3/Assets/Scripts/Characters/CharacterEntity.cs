using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class CharacterEntity : MonoBehaviour
{
    public virtual void PassiveAbility()
    {
        
    }
    public virtual void PrimaryAttack(Transform playerTransform)
    {
        
    }

    public virtual void SpecialAttack()
    {
        
    }

    public virtual void EvasiveManeuver()
    {
        
    }
}
