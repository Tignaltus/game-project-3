using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using System.Threading.Tasks;
using Photon.Pun;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

public class Warrior : PlayerController
{
    [TabGroup("Warrior")] [SerializeField] private float weaponReach = 2f;
    [TabGroup("Warrior")] [SerializeField] private float weaponAoe = 2f;
    [TabGroup("Warrior")] [SerializeField] private float bashStunDuration;
    [TabGroup("Warrior")] [SerializeField] private float shieldBashDuration = 3f;
    [TabGroup("Warrior")] [SerializeField] private float shieldBashCD = 5f;
    [TabGroup("Warrior")] [SerializeField] private float bashSpeedIncrease = 10f;
    [TabGroup("Warrior")] [SerializeField] private int bashDmg;
    [TabGroup("Warrior")] [SerializeField] private GameObject bubble;
    [TabGroup("Warrior")] [SerializeField] private int bubbleMaxHp;
    [TabGroup("Warrior")] [SerializeField] private int bubbleRegenAmount;
    [TabGroup("Warrior")] [SerializeField] private float bubbleRegenRate;
    [TabGroup("Warrior")] [SerializeField] [ColorUsage(true, true)] private Color bubbleMaxDmgColor;
    [TabGroup("Warrior")] [SerializeField] private ParticleSystem sparks;
    [ColorUsage(true, true)] private Color defaultColor;
    [ColorUsage(true, true)] private Color colorLerp;
    private Material bubbleShader;
    private bool startBubble;
    private int bubbleHP;
    private int tempHP;
    private Transform playerTrans;
    private bool attacking;
    private bool specialCD;
    private Collider[] enemyColliders;

    public override void PassiveAbility()
    {
        
    }
    public override void PrimaryAttack(Transform playerTransform)
    {
        //if (attacking) return;
        comboAnims.StartCombo();
        rotateAttack = true;
        //rotateAttack = true;
        //attacking = true;
        //StartCoroutine(AttackRate());
    }

    public override void SpecialAttack()
    {
        if (attacking || specialCD) return;
        attacking = true;
        rotateAttack = true;
        specialCD = true;
        if (photonView.IsMine)
        {
            AudioManager.instance.PlaySFX(audio.specialAttack, GetRandomPitch());
        }

        StartCoroutine(SpecialCooldown());
    }

    public override void EvasiveManeuver()
    {
        if (!startBubble)
        {
            photonView.RPC("StartBubble", RpcTarget.All);

            StartCoroutine(BubbleRegen());
            startBubble = true;
        }

        /*if (bubble.activeSelf)
        {
            bubble.SetActive(false);
            health -= bubbleHP;
        }
        else
        {
            bubble.SetActive(true);
            tempHP = health;
            health += bubbleHP;
            bubbleHP = health - tempHP;
            colorLerp = Color.Lerp(bubbleMaxDmgColor, defaultColor, (float) bubbleHP / bubbleMaxHp);
            bubbleShader.SetColor("Color_Emission", colorLerp);
        }*/
        
        photonView.RPC("RPCBubble", RpcTarget.AllViaServer, bubble.activeSelf, false);
        
        if (photonView.IsMine)
        {
            AudioManager.instance.PlaySFX(audio.EvasiveManouver, 1f);
        }
    }

    public override void ClassUpdate()
    {
        if (customInput)
        {
            moveDir = new Vector3(Input.GetAxisRaw("Horizontal"), 0, 1).normalized;
            Debug.Log(moveDir);
        }
    }

    [PunRPC]
    private void StartBubble()
    {
        bubbleHP = bubbleMaxHp;
        bubbleShader = bubble.GetComponent<Renderer>().material;
        defaultColor = bubbleShader.GetColor("Color_Emission");
    }
    
    [PunRPC]
    private void RPCBubble(bool state, bool bubbleDead)
    {
        if (bubbleDead) {bubble.SetActive(false); bubbleHP = 0; return;}
        
        bubble.SetActive(!state);

        if (!state)
        {
            tempHP = health;
            health += bubbleHP;
            bubbleHP = health - tempHP;
            colorLerp = Color.Lerp(bubbleMaxDmgColor, defaultColor, (float) bubbleHP / bubbleMaxHp);
            bubbleShader.SetColor("Color_Emission", colorLerp);
        }
        else
        {
            health -= bubbleHP;
        }
    }

    /// <summary>
    /// Adds speed based on bashSpeedIncrease. Starts the ShieldBash & ShieldBashDuration coroutine.
    /// If the player attacks again the shieldbash is cancelled or if the ShieldBashDuration time is up.
    /// Reverts everything when done and puts the specialAttack on cooldown. 
    /// </summary>
    /// <returns></returns>
    private IEnumerator SpecialCooldown()
    {
        speedModifier += bashSpeedIncrease;
        animator.SetBool("Special", true);
        customInput = true;
        StartCoroutine(ShieldBash());
        StartCoroutine(ShieldBashDuration());
        yield return new WaitUntil(() => !attacking);
        
        StopCoroutine(ShieldBashDuration());
        attacking = false;
        rotateAttack = false;
        customInput = false;
        speedModifier -= bashSpeedIncrease;
        animator.SetBool("Special", false);

        yield return new WaitForSeconds(shieldBashCD);
        specialCD = false;

    }
    /*private void AttackRate()
    {
        if (PhotonNetwork.IsConnected)
        {
            photonView.RPC("MeleeAttack", RpcTarget.AllViaServer);
        }
        else
        {
            MeleeAttack();
        }
        
        AudioManager.instance.PlaySFX(audio.attack, GetRandomPitch());
    }*/

    private void MeleeAttack()
    {
        if (PhotonNetwork.IsConnected)
        {
            photonView.RPC("MeleeAttackRPC", RpcTarget.AllViaServer);
        }
        else
        {
            MeleeAttack();
        }
        
        AudioManager.instance.PlaySFX(audio.attack, GetRandomPitch());
    }
    
    [PunRPC]
    private void MeleeAttackRPC()
    {
        Debug.Log("attack");

        enemyColliders = Physics.OverlapSphere(transform.position + transform.forward * weaponReach, weaponAoe, targetLayers);

        foreach (var collider in enemyColliders)
        {
            if (collider == myCollider) continue;
            
            collider.GetComponent<IDamageable>().TakeDamage(power, gameObject);
            if(collider.TryGetComponent<AggroTarget>(out var aggroUnit))
            {
                aggroUnit.IncreaseAggro(this.gameObject, 20);
            }
            Debug.Log("Dealt damage");

            var sparkPS = Instantiate(sparks, collider.transform.position, collider.transform.rotation);
            Destroy(sparkPS, 1f);
        }
        
        //AudioManager.instance.PlaySFX(audio.attack, GetRandomPitch());
        //rotateAttack = false;
        attacking = false;
        Debug.Log(attacking);
    }
    
    private IEnumerator ShieldBash()
    {
        var colliders = new List<Collider>();
        
        while (attacking)
        {
            if (Physics.SphereCast(transform.position, 2f, transform.forward, out RaycastHit hit, 2f, targetLayers))
            {
                if (!colliders.Contains(hit.collider) && hit.collider != myCollider)
                {
                    hit.collider.gameObject.GetComponent<IDamageable>().TakeDamage(bashDmg, gameObject);
                    if (hit.collider.gameObject.TryGetComponent<IStunable>(out var stunable))
                    {
                        stunable.Stun(bashStunDuration);
                    }
                }

                colliders.Add(hit.collider);
                Debug.Log("Hit enemy with shield");
            }

            yield return new WaitForSeconds(.1f);
        }
        
        Debug.Log("cancelled");
    }

    public void OnBubbleTakeDamage()
    {
        if (!bubble.activeSelf) return;
        
        bubbleHP = health - tempHP;
        
        colorLerp = Color.Lerp(bubbleMaxDmgColor, defaultColor, (float) bubbleHP / bubbleMaxHp);
        bubbleShader.SetColor("Color_Emission", colorLerp);

        if (bubbleHP < 1)
        {
            photonView.RPC("RPCBubble", RpcTarget.AllViaServer, bubble.activeSelf, true);
        }
    }

    private IEnumerator BubbleRegen()
    {
        while (true)
        {
            if (!bubble.activeSelf && bubbleHP < bubbleMaxHp)
            {
                bubbleHP += bubbleRegenAmount;
            }
            yield return new WaitForSeconds(bubbleRegenRate);
        }
    }

    private IEnumerator ShieldBashDuration()
    {
        yield return new WaitForSeconds(shieldBashDuration);
        attacking = false;
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position + transform.forward * weaponReach, weaponAoe);
    }
}