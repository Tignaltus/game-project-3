using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckIfInArena : MonoBehaviour
{
    [SerializeField] protected float areaRange = 20f;
    [SerializeField] private LayerMask targetLayer = 8;

    public bool InsideCircle()
    {
        var colliders = Physics.OverlapSphere(transform.position, areaRange, targetLayer);

        foreach (var collider in colliders)
        {
            if (collider == PlayerController.LocalPlayerInstance.GetComponent<Collider>())
            {
                Debug.Log("Inside circle");
                return true;
            }
        }
        
        Debug.Log("Outside Circle");
        return false;
    }
    
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, areaRange);
    }
}
