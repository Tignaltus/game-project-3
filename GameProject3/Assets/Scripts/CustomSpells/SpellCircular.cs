using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellCircular : MonoBehaviour
{
   [SerializeField]private Rigidbody rb;
   [SerializeField]private float force;
   [SerializeField]private float rotationForce;
   private Vector3 player;

   private void Start()
   {
      //player = PlayerController.LocalPlayerInstance.transform;
      player = transform.position;
      Destroy(rb.gameObject, 14f);
   }

   private void FixedUpdate()
   {
      Vector3 direction = player - rb.position;
      direction.Normalize();
      Vector3 rotationAmount = Vector3.Cross(transform.right, direction);
      rb.angularVelocity = rotationAmount * rotationForce;
      rb.velocity = transform.right * force;
   }
}
