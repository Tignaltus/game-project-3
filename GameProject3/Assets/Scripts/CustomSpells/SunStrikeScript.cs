using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class SunStrikeScript : MonoBehaviour
{
    [SerializeField] private float RadiusSpawn;

    private void Start()
    {
        transform.position = transform.position + new Vector3(Random.Range(-RadiusSpawn, RadiusSpawn), 0f,
            Random.Range(-RadiusSpawn, RadiusSpawn));
        Destroy(gameObject, 1f);
    }
}
