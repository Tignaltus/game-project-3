using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamageable
{
    public void TakeDamage(int damage, GameObject attacker);
    public bool isDead(int hp);
}
