using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using Random = UnityEngine.Random;

public class MonsterManager : MonoBehaviourPunCallbacks
{
    public List<GameObject> spawnedMonster;
    
    [Serializable]
    public class MonsterSpawner
    {
        public GameObject prefab;
        public List<Transform> spawnArea;
        public int amount;
        public bool spawned;
    }

    [SerializeField] private List<MonsterSpawner> monsterList;

    private void Start()
    {
        StartCoroutine(WaitTilArena());
    }

    private void Update()
    {
        if (!PhotonNetwork.IsMasterClient) return;
        
        foreach (var monsterSpawner in monsterList)
        {
            if(monsterSpawner.spawned) continue;
            
            foreach (var spawnArea in monsterSpawner.spawnArea)
            {
                foreach (var player in GameManager.Instance.spawnedPlayers)
                {
                    if(player == null) continue;
                    
                    if (Vector3.Distance(spawnArea.position, player.transform.position) < spawnArea.gameObject.GetComponent<MonsterArea>().AreaRadius * 7)
                    {
                        monsterSpawner.spawned = true;
                    
                        for (int i = 0; i < monsterSpawner.amount; i++)
                        {
                            var monster = PhotonNetwork.InstantiateRoomObject(monsterSpawner.prefab.name, new Vector3(
                                    spawnArea.position.x + Random.Range(-spawnArea.gameObject.GetComponent<MonsterArea>().AreaRadius, spawnArea.gameObject.GetComponent<MonsterArea>().AreaRadius), spawnArea.position.y, 
                                    spawnArea.position.z + Random.Range(-spawnArea.gameObject.GetComponent<MonsterArea>().AreaRadius, spawnArea.gameObject.GetComponent<MonsterArea>().AreaRadius)), Quaternion.identity, 0);
                        
                            monster.GetComponent<AIEntity>().ServerStart(spawnArea.gameObject.GetComponent<MonsterArea>().AreaRadius, spawnArea.position);
                            
                            spawnedMonster.Add(monster);
                        }
                    }   
                }
            }
        }
    }

    private IEnumerator WaitTilArena()
    {
        yield return new WaitUntil(() => GameManager.Instance.arenaPhase);
        
        foreach (var monster in spawnedMonster)
        {
            PhotonNetwork.Destroy(monster);
        }
        
        spawnedMonster.Clear();
    }
}
