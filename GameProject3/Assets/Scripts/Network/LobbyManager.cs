using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class LobbyManager : MonoBehaviourPunCallbacks
{

    public TMP_InputField roomInputField;

    public GameObject lobbyPanel;

    public GameObject roomPanel;

    public GameObject startGameButton;

    public RoomItem roomItemPrefab;
    private List<RoomItem> roomItemsList = new List<RoomItem>();
    public Transform contentObject;

    public float timeBetweenUpdates = 1.5f;
    private float nextUpdateTime;
    
    public LobbyPlayerItem playerItemPrefab;
    private List<LobbyPlayerItem> playerItemList = new List<LobbyPlayerItem>();
    public Transform playerContentObject;

    public Text roomName;
    
    public UnityEvent OnRoomJoin;
    private void Awake()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    void Start()
    {
        PhotonNetwork.JoinLobby();
    }

    public void OnClickCreate()
    {
        if (roomInputField.text.Length >= 1)
        {
            PhotonNetwork.CreateRoom(roomInputField.text, new RoomOptions(){MaxPlayers = 4});
            OnRoomJoin.Invoke();
            startGameButton.SetActive(true);
        }
    }

    //When joining a room check all players in the room and add them to the lobby.
    public override void OnJoinedRoom()
    {
        lobbyPanel.SetActive(false);
        roomPanel.SetActive(true);
        roomName.text = "Room: " + PhotonNetwork.CurrentRoom.Name;

        foreach (var item in playerItemList)
        {
            Destroy(item.gameObject);
        }
        playerItemList.Clear();
        
        foreach (var player in PhotonNetwork.CurrentRoom.Players)
        {
            var playerItem = Instantiate(playerItemPrefab, playerContentObject);
            playerItem.SetPlayerName(player.Value.NickName);
            playerItemList.Add(playerItem);
        }

        Debug.Log("Joined Room");
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        if (Time.time >= nextUpdateTime)
        {
            UpdateRoomList(roomList);
            nextUpdateTime = Time.time + timeBetweenUpdates;
        }
    }

    //When a player enters the room add that player to the list
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        var playerItem = Instantiate(playerItemPrefab, playerContentObject);
        playerItem.SetPlayerName(newPlayer.NickName);
        playerItemList.Add(playerItem);
        Debug.Log(newPlayer.NickName + "Joined");
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        foreach (var playerItem in playerItemList)
        {
            if (otherPlayer.NickName == playerItem.playerName.text)
            {
                Destroy(playerItem.gameObject);
            }
        }
    }

    private void UpdateRoomList(List<RoomInfo> list)
    {
        foreach (var item in roomItemsList)
        {
            Destroy(item.gameObject);
        }
        roomItemsList.Clear();

        foreach (var room in list)
        {
            RoomItem newRoom = Instantiate(roomItemPrefab, contentObject);
            newRoom.SetRoomName(room.Name);
            roomItemsList.Add(newRoom);
        }
    }

    public void JoinRoom(string roomName)
    {
        PhotonNetwork.JoinRoom(roomName);
        OnRoomJoin.Invoke();
    }

    public void OnClickLeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

    public override void OnLeftRoom()
    {
        roomPanel.SetActive(false);
        lobbyPanel.SetActive(true);
    }

    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby();
    }

    public void OnStartGame()
    {
        PhotonNetwork.LoadLevel(1);
    }

    public void OnQuitGame()
    {
        if (PhotonNetwork.IsConnected)
        {
            PhotonNetwork.Disconnect();
        }
        
        Application.Quit();
    }
    
    public void OnClickDisconnect()
    {
        PhotonNetwork.Disconnect();
    }
}
