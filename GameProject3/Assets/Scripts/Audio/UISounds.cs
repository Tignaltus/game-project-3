using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISounds : MonoBehaviour
{
    [SerializeField] private AudioClip click;
    [SerializeField] private AudioClip hover;

    public void SoundOnClick()
    {
        AudioManager.instance.PlayUISound(click);
    }
    
    public void SoundOnHover()
    {
        AudioManager.instance.PlayUISound(hover);
    }
}
