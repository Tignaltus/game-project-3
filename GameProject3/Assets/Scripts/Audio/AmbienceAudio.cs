using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbienceAudio : AudioEntity
{
    public AudioSource ambientSource;

    private void Start()
    {
        StartCoroutine(PlaySoundArea());
    }

    private IEnumerator PlaySoundArea()
    {
        while (true)
        {
            yield return new WaitUntil(() => InAudioRange());
            AudioManager.instance.PlayAmbience(ambientSource, true);
            
            yield return new WaitUntil(() => !InAudioRange());
            AudioManager.instance.PlayAmbience(ambientSource, false);
        }
    }
    
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, audioRange);
    }
}
