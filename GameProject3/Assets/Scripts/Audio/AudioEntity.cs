using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Sirenix.OdinInspector;
using UnityEngine;

public class AudioEntity : MonoBehaviour
{
    [TabGroup("General")]
    public AudioClip hit;
    [TabGroup("General")]
    public AudioClip death;
    [TabGroup("General")]
    public AudioClip attack;
    [TabGroup("General")]
    public AudioClip stunned;
    [TabGroup("Player")]
    public AudioClip specialAttack;
    [TabGroup("Player")]
    public AudioClip footsteps;
    [TabGroup("Player")]
    public AudioClip footsteps2;
    [TabGroup("Player")]
    public AudioClip jump;
    [TabGroup("Player")]
    public AudioClip EvasiveManouver;

    [SerializeField] protected float audioRange = 20f;
    [SerializeField] private LayerMask targetLayer = 8;

    public bool InAudioRange()
    {
        var colliders = Physics.OverlapSphere(transform.position, audioRange, targetLayer);

        foreach (var collider in colliders)
        {
            if (collider == PlayerController.LocalPlayerInstance.GetComponent<Collider>())
            {
                return true;
            }
        }

        return false;
        //return Physics.CheckSphere(transform.position, audioRange, targetLayer);
    }
}
