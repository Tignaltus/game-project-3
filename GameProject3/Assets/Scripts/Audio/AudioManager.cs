using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
    [TabGroup("AudioSource")]
    public AudioSource UISound;
    [TabGroup("AudioSource")]
    public AudioSource SFX;
    [TabGroup("AudioSource")]
    public AudioSource Music;
    [TabGroup("AudioSource")]
    public AudioSource FootSteps;
    [TabGroup("AudioSource")]
    public AudioSource Bell;
    [TabGroup("AudioSource")]
    public AudioSource Ambience;

    [TabGroup("Menu")]
    [SerializeField] private Slider masterSlider;
    [TabGroup("Menu")]
    [SerializeField] private Slider SFXSlider;
    [TabGroup("Menu")]
    [SerializeField] private Slider UISlider;
    [TabGroup("Menu")]
    [SerializeField] private Slider musicSlider;
    [TabGroup("Menu")]
    [SerializeField] private Slider ambienceSlider;

    [TabGroup("Menu")]
    [SerializeField] private TMP_Text masterText;
    [TabGroup("Menu")]
    [SerializeField] private TMP_Text SFXText;
    [TabGroup("Menu")]
    [SerializeField] private TMP_Text UIText;
    [TabGroup("Menu")]
    [SerializeField] private TMP_Text MusicText;
    [TabGroup("Menu")]
    [SerializeField] private TMP_Text ambienceText;
    
    [Range(0, 1f)]
    private float masterVolume;

    public static AudioManager instance;

    private void Awake()
    {
        DontDestroyOnLoad(this);
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);   
        }
    }

    public void PlayUISound(AudioClip clip)
    {
        UISound.clip = clip;
        UISound.Play();
    }
    
    public void PlaySFX(AudioClip clip, float pitch)
    {
        SFX.pitch = pitch;
        SFX.PlayOneShot(clip);
    }
    
    public void PlayMusic(AudioClip clip, float volume)
    {
        Music.volume = Mathf.Lerp(0f, Music.volume, volume);
        Music.clip = clip;
        Music.Play();
    }
    
    public void PlayAmbience(AudioSource source, bool statement)
    {
        if (statement)
        {
            source.volume = Ambience.volume;
            source.Play();
        }
        else
        {
            source.Pause();
        }
    }

    public void PlayFootSteps(AudioClip clip)
    {
        FootSteps.clip = clip;
        FootSteps.Play();
    }

    public void PlayBell(AudioClip clip)
    {
        Bell.clip = clip;
        Bell.Play();
    }

    public void OnVolumeChange(string name)
    {
        switch (name)
        {
            case "Master":
                masterVolume = masterSlider.value;
                masterText.text = Mathf.RoundToInt(masterSlider.value * 100) + "%";
                GetMasterVolume();
                break;
            case "SFX":
                SFX.volume = SFXSlider.value;
                SFXText.text = Mathf.RoundToInt(SFXSlider.value * 100) + "%";
                GetMasterVolume();
                break;
            case "UI":
                UISound.volume = UISlider.value;
                UIText.text = Mathf.RoundToInt(UISlider.value * 100) + "%";
                GetMasterVolume();
                break;
            case "Music":
                Music.volume = musicSlider.value;
                MusicText.text = Mathf.RoundToInt(musicSlider.value * 100) + "%";
                GetMasterVolume();
                break;
            case "Ambience":
                Ambience.volume = ambienceSlider.value;
                ambienceText.text = Mathf.RoundToInt(ambienceSlider.value * 100) + "%";
                GetMasterVolume();
                break;
        }
    }

    private void GetMasterVolume()
    {
        SFX.volume = Mathf.Lerp(0f, SFXSlider.value, masterSlider.value);
        UISound.volume = Mathf.Lerp(0f, UISlider.value, masterSlider.value);
        Music.volume = Mathf.Lerp(0f, musicSlider.value, masterSlider.value);
        Ambience.volume = Mathf.Lerp(0f, ambienceSlider.value, masterSlider.value);
        
        Bell.volume = Mathf.Lerp(0f, 0.4f, SFX.volume);
        FootSteps.volume = Mathf.Lerp(0f, 0.33f, SFX.volume);
    }
}
