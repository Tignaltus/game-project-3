using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Compass : MonoBehaviour
{
    public GameObject iconPrefab;
    List<TownMarker> townMarkers = new List<TownMarker>();

    public RawImage compassImage;
    public Transform player;

    float compassUnit;

    public TownMarker one;
    public TownMarker two;
    public TownMarker three;

    private void Start()
    {
        compassUnit = compassImage.rectTransform.rect.width / 360f;
        player = PlayerController.LocalPlayerInstance.transform;

        AddTownMarker(one);
        //AddTownMarker(two);
        //AddTownMarker(three);
    }

    private void Update()
    {
        compassImage.uvRect = new Rect(player.localEulerAngles.y / 360f, 0f, 1f, 1f);

        foreach (TownMarker marker in townMarkers)
        {
            marker.image.rectTransform.anchoredPosition = GetPosOnCompass(marker);
        }
    }

    public void AddTownMarker (TownMarker marker)
    {
        GameObject newMarker = Instantiate(iconPrefab, compassImage.transform);
        marker.image = newMarker.GetComponent<Image>();
        marker.image.sprite = marker.icon;

        townMarkers.Add(marker);
        
    }

    Vector2 GetPosOnCompass (TownMarker marker)
    {
        Vector2 playerPos = new Vector2(player.transform.position.x, player.transform.position.z);
        Vector2 playerFwd = new Vector2(player.transform.forward.x, player.transform.forward.z);

        float angle = Vector2.SignedAngle(marker.position - playerPos, playerFwd);

        return new Vector2(compassUnit * angle, 0f);
    }
}
